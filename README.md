# Topic Modeling for Humpback Whale Song

### Requirements
 - Python3.6
 - [Docker](https://docs.docker.com/installation/) -  this is needed for the Realtime Online Spatiotemporal Topic Modeling (ROST) library from WHOI's WARP Lab
- [Wandb account](https://www.wandb.com) - this is used to log and visualize the model metrics through the wandb service
- [VGGish model checkpoint](https://storage.googleapis.com/audioset/vggish_model.ckpt)
- [Embedding PCA parameters](https://storage.googleapis.com/audioset/vggish_pca_params.npz)

## Related Work
 - [MBARI ROST Docker](https://bitbucket.org/mbari/rost-docker)
 
### Installing Python Dependencies
The dependencies of this project are listed requirements.txt. The code below will install these
requirements in a virtual environment. This requires virtualenv, if you don't have it install it with 
 `pip install virtualenv`.
```bash
$ pip install virtualenv
$ virtualenv --python=python3.6 .venv
$ source .venv/bin/activate
(venv) $ pip install -r requirements.txt

## Installing VGG dependencies
```
cd src
curl -O https://storage.googleapis.com/audioset/vggish_model.ckpt
curl -O https://storage.googleapis.com/audioset/vggish_pca_params.npz
git submodule add https://github.com/tensorflow/models.git
```
### Grab a test file
```
dvc pull
```
Anaconda install on MBARI production machine, skip for local testing
```.env
conda create --name soundscape-humpback-topicmodel python=3.6
conda activate soundscape-humpback-topicmodel
set -x HDF5_DIR /opt/pam/anaconda/
pip install -r requirements.txt
```

### Setting up the environment
Export the following variables in your shell
```bash
export APP_HOME=<path to this file>
export WANDB_API_KEY=<your api key>
export WANDB_USERNAME=<your username> e.g. dcline
export WANDB_ENTITY=<your company/entity name> e.g. mbari
export WANDB_MODE=run
export WANDB_GROUP=humpback-topicmodel
export WANDB_PROJECT=<your project> e.g 901502-soundscape-unsupervised
```
### Running the Project
Once all dependencies are installed, the project can be run with
```bash
(.venv) $ python src/run_model.py
```
which will run all project modules using the default parameters specified in conf.py.  

You should see an output generate with a sample in the data/artifacts folder
![topic example](./img/topic_example.png)
## Cluster K value model sweeps
Parameters chosen in the conf.py were selected based on model tuning. 

For K, the knee of the inertia curve was chosen based on visual inspection of 
the kmean model sweep `sweep_kmeans.yaml`. 

Set the group to contain the sweeps in and other needed environment parameters for wandb with:
```bash
(.venv) $ export WANDB_GROUP=humpback-topicmodel-kmeans
(.venv) $ export WANDB_ENTITY mbari
(.venv) $ export WANDB_PROJECT 901502-soundscape-unsupervised
(.venv) $ export WANDB_USERNAME dcline
(.venv) $ export WANDB_API_KEY <your api key>
```
Run the model sweep with:
```
(.venv) $ wandb sweep sweeps/sweep_kmeans_pcen.yaml
```
This will print out *your-sweep-id*, e.g. below this is sweep 9v3eplhs. 
```bash
wandb: Creating sweep from: sweeps/sweep_kmeans.yaml
wandb: Created sweep with ID: 9v3eplhs
wandb: View sweep at: https://app.wandb.ai/mbari/soundscape-humpback-topicmodel/src/sweeps/9v3eplhs
wandb: Run sweep agent with: wandb agent mbari/soundscape-humpback-topicmodel/src/9v3eplhs
```
Copy that in the agent command, e.g.
```bash
(.venv) $wandb agent mbari/soundscape-humpback-topicmodel/9v3eplhs
```

## Gamma model sweep
Similarly to the K value model sweeps, Gamma, Alpha, and Beta sweeps
can be performed with:
```bash
(.venv) $ export WANDB_GROUP=humpback-topicmodel-gamma
(.venv) $ wandb sweep sweeps/sweep_gamma.yaml
```
```bash
(.venv) $ export WANDB_GROUP=humpback-topicmodel-alpha
(.venv) $ wandb sweep sweeps/sweep_alpha.yaml
```
```bash
(.venv) $ export WANDB_GROUP=humpback-topicmodel-beta
(.venv) $ wandb sweep sweeps/sweep_beta.yaml
```

## References
* This code began as an [intern project with intern Thomas Bergamaschi](https://www.mbari.org/wp-content/uploads/2018/12/Bergamaschi.pdf).
* ROST: https://gitlab.com/warplab/rost-cli