# Soundscape Humpback Topic Model Changelog

# [1.3.0](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.2.1...v1.3.0) (2020-08-10)


### Features

* **vggish.py:** added vggish model for coherence tests ([d75e674](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/d75e674c235023e509c4ac05ff4b58a539c1692b))

## [1.2.1](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.2.0...v1.2.1) (2020-08-06)


### Bug Fixes

* **visualize.py:** remove scaling ([ed22f48](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/ed22f4899e904d28e0b6b643a5e78e7314f203d1))

# [1.2.0](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.1.4...v1.2.0) (2020-08-05)


### Features

* **visualize.py:** removed max prob plot and increased size of the font for ppt presentation ([ebb8450](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/ebb8450d19f287cbdba477b38192f89393826943))

## [1.1.4](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.1.3...v1.1.4) (2020-08-04)


### Bug Fixes

* **run_similarity.py:** export per wav theta ([d339456](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/d339456ca8ced5abc6ed78788b397330919fe7da))


### Reverts

* **conf.py:** reverted to prior settings before tone tests ([2a9468a](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/2a9468a964f658b93f22126cd87b76bb22e1756f))

## [1.1.3](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.1.2...v1.1.3) (2020-08-04)


### Bug Fixes

* correct subsetting ([a25e210](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/a25e2107e87d0af8cf2c5b1133bbb7348cbb8b18))

## [1.1.2](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.1.1...v1.1.2) (2020-08-04)


### Bug Fixes

* **run_similarity.py:** correct subsetting of theta ([61274fd](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/61274fd43826679712cdfb5c11531ec70a5856da))

## [1.1.1](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.1.0...v1.1.1) (2020-08-03)


### Bug Fixes

* **run_similarity.py:** added support for multiple file plots ([ba8a718](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/ba8a71811f36da3a53ec4980fe5bd5387847303a))

# [1.1.0](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.17...v1.1.0) (2020-07-29)


### Features

* **run_similarity.py:** initial check-in of similarity test ([715aa29](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/715aa292c53063cd5f7aae5791f516f76697300d))

## [1.0.17](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.16...v1.0.17) (2020-07-15)


### Bug Fixes

* **requirements.txt:** fix versions for librosa and number compat ([cf14f8e](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/cf14f8e02265e2a886cb0aa7cefc1320fbf232f5))

## [1.0.16](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.15...v1.0.16) (2020-07-15)


### Bug Fixes

* added missing dvc file ([256a17a](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/256a17a98a1acba536bb8ac5ff58139f53f23e21))

## [1.0.15](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.14...v1.0.15) (2020-07-15)


### Bug Fixes

* **requirements.txt:** added new line ([5a5467c](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/5a5467c5a321ae8ec8c3af4dd31d1fc6a9dae143))

## [1.0.14](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.13...v1.0.14) (2020-07-06)


### Bug Fixes

* **sweeps:** correct g_time sweep for full length file ([c3b8a05](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/c3b8a051a333b878b4abc436078f978a0b97d254))

## [1.0.13](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.12...v1.0.13) (2020-07-03)


### Bug Fixes

* **sweeps:** correct yaml format for g_time ([c78e647](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/c78e6472a1ee9f3a2bf5359fe54bdfe29b186440))

## [1.0.12](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.11...v1.0.12) (2020-07-01)


### Bug Fixes

* **sweep_g_time:** correct python file for sweep ([7489492](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/7489492ead10bb3b74188c609caa7d3955112055))

## [1.0.11](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.10...v1.0.11) (2020-07-01)


### Bug Fixes

* **sweep_g_time.yaml:** removed unused value arg ([36d4d34](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/36d4d34a5848a74ab1bb3021569429872baebe76))

## [1.0.10](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.9...v1.0.10) (2020-06-25)


### Bug Fixes

* **sweeps:** renamed boolean to wandb compatible string ([2ccfb58](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/2ccfb58b27d06ee717a7a949c3b0d2063edcff36))

## [1.0.9](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.8...v1.0.9) (2020-06-23)


### Bug Fixes

* **sweeps:** fixed value for type, otherwise interpreted as single chars by wandb ([ed24cf5](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/ed24cf58d830b763ea990dfb98fe26e142c62387))

## [1.0.8](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.7...v1.0.8) (2020-06-23)


### Bug Fixes

* **download.py:** added model sweep type to the file download ([ac5de52](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/ac5de52d5bdfa098794727e9442014d6d2cf4351))

## [1.0.7](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.6...v1.0.7) (2020-06-23)


### Bug Fixes

* correct title per the run ([c2528d2](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/c2528d2cb10df9d361cf35573b23634b55a2d96b))

## [1.0.6](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.5...v1.0.6) (2020-06-23)


### Bug Fixes

* **requirements.txt:** removed redundant requirement ([b906f8f](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/b906f8f082dd626fd3c4da14953822029f95eb68))

## [1.0.5](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.4...v1.0.5) (2020-06-22)


### Bug Fixes

* **sweeps:** added missing parameter to visualization ([feb7b4b](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/feb7b4b0a4c29c958e36603cce5a3514b726ac1a))

## [1.0.4](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.3...v1.0.4) (2020-06-21)


### Bug Fixes

* added missing doc file dir ([019355b](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/019355b7719fd376cd8208d120c54a509724fe89))

## [1.0.3](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.2...v1.0.3) (2020-06-21)


### Bug Fixes

* **sweep_alpha_beta.py:** added missing model output dir ([48c70fc](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/48c70fc9f33e49f17304be6af19d5d8d45d9f43f))

## [1.0.2](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.1...v1.0.2) (2020-06-21)


### Bug Fixes

* correct path to artifacts ([3141e18](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/3141e187eb19595bff3c1165e4d0528134fa71be))

## [1.0.1](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/compare/v1.0.0...v1.0.1) (2020-06-21)


### Bug Fixes

* **requirements.txt:** pinned to fix numba issue with librosa ([686831a](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/686831ab83c7cef136c254dbd7c0b1430211c880))

# 1.0.0 (2020-06-16)


### Features

* initial check-in from pam-topic-model repo ([b395923](http://bitbucket.org/mbari/soundscape-humpback-topicmodel/commits/b39592335c2ba137dd91bd713968d470930dba68))

# Soundscape Humbpack Topic Model Versioning Changelog
