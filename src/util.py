import os
import subprocess
import csv


def ensure_dir(fname):
    """
    Create directory if does not exist
    :param fname:
    :return:
    """
    d = os.path.dirname(fname)
    if not os.path.exists(d):
        os.makedirs(d)

def minmax_scale(x, x_min, x_max):
    """
    Scale array between max and min
    :param x: array to scale
    :param x_min: minimum value
    :param x_max: maximum value
    :return:
    """
    x_std = (x - x.min(axis=0)) / (x.max(axis=0) - x.min(axis=0))
    x_scaled = x_std * (x_max - x_min) + x_min
    return x_scaled


def ensure_dir(fname):
    d = os.path.dirname(fname)
    if not os.path.exists(d):
        os.makedirs(d)


def write_csv(data, fname):
    ensure_dir(fname)
    with open(fname, "w") as f:
        writer = csv.writer(f, delimiter=',')
        writer.writerows(data)


def execute(cmd):
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    output, error = process.communicate()
    return output, error


def map_range(x, a, b, c=0, d=1):
    y = (x-a) * (d-c)/(b-a) + c
    return y
