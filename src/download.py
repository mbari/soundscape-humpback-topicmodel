#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utility for downloading model sweeps from wandb

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import os
import argparse
import wandb
import tarfile

def download_decompress(tar_filename):
	print(f'Downloading {tar_filename}')
	run.file(tar_filename).download(replace=True)
	target_dir = os.path.join(os.getcwd(), run.id)
	print(f'Decompressing {tar_filename} to {target_dir}')
	with tarfile.open(tar_filename) as tar:
		if not os.path.exists(target_dir):
			os.makedirs(target_dir)
		tar.extractall(path=target_dir)

if __name__ == '__main__':
	parser = argparse.ArgumentParser('')
	parser.add_argument('--run', type=str)
	parser.add_argument('--sweep', type=str)
	parser.add_argument('--type', type=str, required=True, help='alpha, beta or gamma')
	args = parser.parse_args()

	api = wandb.Api()

	if args.run:
		run = api.run(f'mbari/901502-soundscape-unsupervised/{args.run}')
		try:
			tar_filename = f'{run.id}_{args.type}_model_best.tar.gz'
			download_decompress(tar_filename)
		except Exception as ex:
			print(f'Error {ex}')

	if args.sweep:
		runs = api.sweep(f'mbari/901502-soundscape-unsupervised/{args.sweep}')
		for run in runs:
			print(run)
			try:
				tar_filename = f'{run.id}_{args.type}_model_best.tar.gz'
				download_decompress(tar_filename)
			except Exception as ex:
				print(f'Error {ex}')
