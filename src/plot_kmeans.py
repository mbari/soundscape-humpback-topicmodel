#!/usr/bin/env python
__author__ = 'Danelle Cline,Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Kmeans sweep plots

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import os
import sys
import cluster
import csv
import numpy as np
import pandas as pd
import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import conf
import glob

if __name__ == "__main__":

        # Combine into one plot
        # start a new figure - size is in inches
        fig = plt.figure(figsize=(8, 11))
        ax1 = plt.subplot()

        plt.yscale('log')
        ax1.set_ylabel('Inertia')
        ax1.set_xlabel('K')
        ax1.set_title('Inertia vs K value', fontstyle='italic')

        method_markers = {'PCEN': 'o'}
        window_color = {1024: 'c', 2048: 'm', 4096: 'k'}
        overlap_sz = {0.5: 10, 0.95: 60}
        markers = []
        names = []

        df = pd.DataFrame()
        for name, marker in method_markers.items():
            s = plt.Line2D((0, 1), (0, 0), color='grey', marker=marker, linestyle='')
            names.append(name)
            markers.append(s)

        for window, color in sorted(window_color.items()):
            s = plt.Line2D((0, 1), (0, 0), color=color, marker='_', linestyle='')
            names.append('FFT window size {}'.format(window))
            markers.append(s)
        ax1.legend(markers, names, loc=0)

        inc = 500
        ax1.text(750, inc, r'FFT overlap', fontsize=8)
        for overlap, pt_size in sorted(overlap_sz.items()):
            ax1.text(760, inc - 150, r'{0}'.format(overlap), fontsize=8)
            c = mpatches.Circle((710, inc - 150), pt_size / 5, edgecolor='black', facecolor='black')
            ax1.add_patch(c)
            inc -= 200

        with open(os.path.join(conf.out_dir, 'kmeans_analysis.csv'), 'w') as f:
            for g in sorted(glob.iglob(conf.out_dir + '**/*_kmeans_analysis.csv', recursive=True)):
                df = pd.read_csv(g)
                m = '.'
                c = 'grey'
                s = 100
                f = os.path.basename(g).split('_')
                window = int(f[1]);
                method = f[0];
                overlap = float(f[2])
                if method in method_markers.keys():
                    m = method_markers[method]
                if window in window_color.keys():
                    c = window_color[window]
                if overlap in overlap_sz.keys():
                    s = overlap_sz[overlap]

                l = os.path.basename(g)
                print('{} {}'.format(l, df.values.astype(int) / 1e5))
                ax1.scatter(df.keys().astype(int), df.values[0].astype(int) / 1e5, marker=m, color=c, s=s, label=l)
                ax1.plot(df.keys().astype(int), df.values[0].astype(int) / 1e5, color=c, label=l, linewidth=1)

        fig.savefig(os.path.join(conf.out_dir, 'Inertia.png'), bbox_inches='tight')
