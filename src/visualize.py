#!/usr/bin/env python
__author__ = 'Danelle Cline, Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utilities for plotting topic model output

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import librosa
from librosa import display
import numpy as np
import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt
import conf
import os

font = {'family': 'Times',
        'weight': 'normal',
        'size': 24}

matplotlib.rc('font', **font)  # font for everything
plt.rc('legend', fontsize=18)  # smaller font for legend


def stacked_bar(data, legend_title="Topic"):
    data_length = len(data)
    data_height = len(data[0])
    P = []
    ind = np.arange(data_length)
    bottom = np.zeros(len(data))
    width = 1

    for z in range(data_height):
        col = [row[z] for row in data]
        P.append(plt.bar(x=ind, height=col, width=width, bottom=bottom, align='edge'))
        bottom = np.add(col, bottom)
    plt.xlim(xmin=0, xmax=len(data))
    max_topic_shown = min(len(P), 10)
    plt.legend(tuple(_[0] for _ in P[0:max_topic_shown]),
               [f'{legend_title}{i}' for i in range(data_height)],
               loc='upper center', bbox_to_anchor=(0.5, 1.15), ncol=10, fancybox=True, shadow=True)

    return plt.gca()


def bar(data):
    data_length, data_height = data.shape
    x = np.arange(data_length)
    y = [i.max() for i in data]
    topics = [int(np.argmax(i)) for i in data]

    # get default colors
    default_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    num_colors = len(default_colors)
    plt.bar(x, y, width=1, color=[default_colors[t % num_colors] for t in np.array(topics)])

    plt.xlim(xmin=0, xmax=len(data))
    plt.ylim(ymin=0, ymax=1.0)
    return plt.gca()


def plot(title, stft, metadata, theta, theta_max_prob, target_file=conf.default_topic_output_file, freq_range=conf.freq_range):

    stft = np.matrix.transpose(stft)
    fig = plt.figure(figsize=(20, 10))
    plt.subplot(2, 1, 1)
    librosa.display.specshow(stft, sr=metadata.sample_rate, x_axis='time', y_axis='mel', cmap='Blues', fmin=freq_range[0],
                             fmax=freq_range[1], hop_length=metadata.hop_length)
    plt.xlabel("Time")
    plt.title(title)

    plt.subplot(2, 1, 2)
    stacked_bar(theta)
    plt.xlabel("Documents")
    plt.ylabel("Topic Probability")

    # plt.subplot(3, 1, 3)
    # bar(theta_max_prob)
    # plt.xlabel("Smoothed Documents")
    # plt.ylabel("Max Topic Probability")

    plt.tight_layout()
    print(f'Saving {target_file}')
    fig.savefig(target_file, dpi=120)
    return plt


if __name__ == "__main__":
    from data import humpback_songsession as song
    import tempfile
    import tarfile

    dataset = song.Dataset()

    with tempfile.TemporaryDirectory(dir='/tmp') as tmp_dir:
        with tarfile.open(conf.default_model_file) as tar:
            tar.extractall(path=tmp_dir)

        theta_path = os.path.join(tmp_dir, 'theta.csv')
        plot("test", dataset, dataset.metadata.iloc[0], theta_path)
