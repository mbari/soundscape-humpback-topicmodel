#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

High-level module for running the topic model

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import gen_documents
import os
import visualize
import conf
import cluster
import model
import tempfile
import tarfile
import pandas as pd
import shutil
from data import humpback_songsession as song

if __name__ == '__main__':
    print('Creating song dataset...')
    dataset = song.Dataset()

    print('Clustering...')
    vocab_size = conf.vocab_size
    t = cluster.Cluster(dataset, num_clusters=vocab_size)
    t.start()
    t.join()

    print('Creating documents...')
    d = gen_documents.Document(metadata=dataset.metadata)
    d.start()
    d.join()

    print('Running topic model...')
    t = model.TopicModel(vocab_size=vocab_size)
    t.start()
    t.join()

    with tempfile.TemporaryDirectory() as tmp_dir_in:
        with tarfile.open(conf.default_model_file) as tar:
            tar.extractall(path=tmp_dir_in)

        theta = pd.read_csv(os.path.join(tmp_dir_in, 'theta.csv'), header=None)
        theta_max_prob = pd.read_csv(os.path.join(tmp_dir_in, 'theta_max_prob.csv'), header=None)
        ms_per_word = int(((dataset.metadata.hop_length / dataset.metadata.sample_rate) * 1000))
        title = f'{conf.target_file_test}, {conf.seconds_range[0]} to {conf.seconds_range[1]} secs, msec per word {ms_per_word}'
        plot = visualize.plot(title, dataset, dataset.metadata.iloc[0], theta, theta_max_prob,
                              seconds_range=conf.seconds_range, target_file=conf.default_topic_output_file)

        with tempfile.TemporaryDirectory() as tmp_dir_out:
            print(f'Compressing {tmp_dir_in} to {conf.default_model_file}...')
            shutil.copy2(conf.default_topic_output_file, os.path.join(tmp_dir_in, 'topic_model.png'))
            with tarfile.open(conf.default_model_file, "w:gz") as tar:
                tar.add(tmp_dir_in, arcname='.')
