#!/usr/bin/env python
__author__ = 'Danelle Cline, Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Preprocess sound files for input to the topic model

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import soundfile as sf
import math
import pandas as pd
import glob
import librosa
import librosa.display
import os
from collections import OrderedDict
import time
import numpy as np
import cv2 as cv
import conf
from scipy.ndimage import gaussian_filter
from util import ensure_dir, minmax_scale
from scipy.ndimage.filters import convolve
import tarfile
import matplotlib.pyplot as plt
import tempfile


class Preprocess:

    def __init__(self, out_file, in_dir=conf.wav_path, window_size=conf.window_size, overlap=conf.overlap,
                 freq_range=conf.freq_range, sigma=conf.sigma, gain=conf.pcen_gain, bias=conf.pcen_bias,
                 power=conf.pcen_power, time_constant=conf.pcen_time_constant, epsilon=conf.pcen_epsilon,
                 duration=conf.coherence_duration, bandwidth=conf.coherence_bandwidth):

        # Check whether output path exists and if not create
        ensure_dir(os.path.abspath(out_file))

        # Declare file number and timer variables for
        # keeping track of run time and dataset statistics.
        num_files = 0
        read_time = 0
        fft_time = 0
        df_conv_time = 0
        hdf_time = 0
        tar_filenames = []
        hop_length = int(window_size * (1 - overlap))

        if conf.single_file_test:
            file_pattern = conf.target_file_test
        else:
            file_pattern = conf.file_pattern

        with tempfile.TemporaryDirectory() as tmp_dir:
            # Preprocess all wav files in the directory specified by in_dir.
            length_index = 0
            for filename in sorted(glob.iglob(os.path.join(in_dir, file_pattern))):
                metadata = dict()

                num_files += 1

                # Read wav and store the filename without its stem.
                print(f'Reading file {filename}...')
                t_0 = time.time()
                name = filename.split('/')[-1]
                name = name.split('.')[0]

                x, _ = librosa.load(filename, sr=None)
                info = sf.info(filename)

                # Capture some metadata for later use in visualizing and extracting topics
                metadata['filename'] = os.path.basename(filename)
                metadata['duration'] = info.duration
                metadata['frames'] = len(x)
                metadata['length_index'] = length_index
                metadata['window_size'] = window_size
                metadata['hop_length'] = hop_length
                metadata['sample_rate'] = info.samplerate

                length_index += len(x) / info.samplerate
                t_1 = time.time()
                read_time += (t_1 - t_0)
                print('Done. (%f seconds)' % (t_1 - t_0))

                print(f'Computing spectrogram with window size {window_size} hop length {hop_length} '
                      f'for {freq_range[0]} - {freq_range[1]} Hz...')
                t_0 = time.time()

                x = gaussian_filter(x, sigma=sigma)
                if conf.use_pcen:
                    stft_float64 = self.compute_pcen(x, overlap=overlap, window_size=window_size, info=info,
                                             power=power, gain=gain, bias=bias, time_constant=time_constant,
                                             epsilon=epsilon, freq_range=freq_range)
                    stft = stft_float64
                else:
                    stft = self.compute_stft(x, window_size=window_size, overlap=overlap)
                    stft = self.get_subset(stft, freq_range, info.samplerate)

                t_1 = time.time()
                fft_time += (t_1 - t_0)
                print(f'Done. ({t_1 - t_0} seconds)')

                t_0 = time.time()
                # Apply Gaussian filter if sigma is specified.
                if sigma is not None:
                    print(f'Smoothing with Gaussian filter sigma={sigma}...')
                    stft = gaussian_filter(stft, sigma=sigma)

                if conf.use_coherence:
                    stft = minmax_scale(stft, -1.,  1.).astype('float32')
                    print(f'Denoising with coherence {int(duration)},{int(bandwidth)} filter ...')
                    c_filter = self.coherence_filter_cv(stft, duration=duration, bandwidth=bandwidth)
                    t_1 = time.time()
                    fft_time += (t_1 - t_0)
                    print(f'Done. ({t_1 - t_0} seconds)')

                    scale_factor = 1.0
                    enhanced_stft = minmax_scale(stft * scale_factor * c_filter, 0., 1.)
                else:
                    enhanced_stft = stft

                # Raw spectrogram #############################
                # plt.subplot(3, 1, 1)
                # librosa.display.specshow(enhanced_stft, x_axis='time', y_axis='linear',hop_length=hop_length,
                #                          fmin=freq_range[0], fmax=freq_range[1], sr=info.samplerate)
                # plt.title('enhanced_stft')
                # plt.colorbar()
                #
                # plt.subplot(3, 1, 2)
                # librosa.display.specshow(c_filter, x_axis='time', y_axis='linear',hop_length=hop_length,
                #                          fmin=freq_range[0], fmax=freq_range[1], sr=info.samplerate)
                # plt.title('c_filter')
                # plt.colorbar()
                #
                # plt.subplot(3, 1, 3)
                # librosa.display.specshow(minmax_scale(stft, 0., 1.), x_axis='time', y_axis='linear', hop_length=hop_length,
                #                          fmin=freq_range[0], fmax=freq_range[1], sr=info.samplerate)
                # plt.title('stft')
                # plt.colorbar()
                # plt.tight_layout()
                # plt.show()

                # Convert the spectrogram from ndarray to DataFrame with time step indexes and frequency bin columns.
                print('Converting to dataframe...')
                t_0 = time.time()
                df = self.stft_to_dataframe(enhanced_stft)
                metadata['length'] = enhanced_stft.shape[1]
                t_1 = time.time()
                df_conv_time += (t_1 - t_0)
                print(f'Done. ({t_1 - t_0} seconds)')

                # Dump the dataframe as a h5
                t_0 = time.time()
                f = os.path.join(tmp_dir, f'{name}.h5')
                print(f'Saving to {f} ...')
                tar_filenames.append(f)
                df.to_hdf(f, key='spec_df', mode='w')

                # write out song metadata for later visualization and extraction of topics
                s = pd.Series(metadata, name='metadata')
                s.index.name = 'metadata'
                s.to_hdf(f, key='metadata_df')
                t_1 = time.time()
                hdf_time = (t_1 - t_0)
                print(f'Done. {hdf_time} seconds)')

            # compress all the results
            print(f'Compressing {tmp_dir} to {out_file}...')
            with tarfile.open(out_file, "w:gz") as tar:
                tar.add(tmp_dir, arcname='.')

        # Report that preprocessing has been completed and
        # output runtime and dataset statistics.
        print(f'Read {num_files} files')
        print(f'Total runtime: {read_time + fft_time + df_conv_time + hdf_time} seconds')
        print('Done')

    @staticmethod
    def compute_pcen(signal, window_size, overlap, freq_range, info, gain, bias, power,
                     time_constant, epsilon):
        hop_length = int(window_size * (1 - overlap))
        print(f'Computing spectrogram with window size {window_size} hop length {hop_length} '
              f'for {freq_range[0]} - {freq_range[1]} Hz...')
        s = librosa.feature.melspectrogram(signal,
                                               n_fft=window_size, hop_length=hop_length,
                                               sr=info.samplerate, power=1, fmin=freq_range[0],
                                               fmax=freq_range[1])

        print(f'Normalizing using PCEN gain {gain} time constant {time_constant} power {power} bias {bias}...')
        pcen_s = librosa.pcen(s, sr=info.samplerate, hop_length=hop_length, gain=gain,
                            time_constant=time_constant, power=power, bias=bias, eps=epsilon)
        return pcen_s

    @staticmethod
    def compute_stft(signal, window_size, overlap, power=False):
        """
        Compute the spectrogram of the signal.

        :param signal: signal to compute spectrogram from
        :param window_size: number of samples per FFT
        :param overlap: overlap of each window
        :param freq_range: range in frequency to subset
        :param power: if power=True, the power spectrum is returned
        :returns: the computed spectrogram
        """
        hop_length = int(window_size * (1 - overlap))
        stft = np.abs(librosa.stft(y=signal, n_fft=window_size, hop_length=hop_length))
        if power:
            stft = librosa.amplitude_to_db(stft, ref=np.max)
        return stft

    @staticmethod
    def coherence_filter_cv(img, duration, bandwidth):
        imgDiffX = cv.Sobel(img, cv.CV_32F, 1, 0, 3)
        imgDiffY = cv.Sobel(img, cv.CV_32F, 0, 1, 5)
        imgDiffXY = cv.multiply(imgDiffX, imgDiffY)
        imgDiffXX = cv.multiply(imgDiffX, imgDiffX)
        imgDiffYY = cv.multiply(imgDiffY, imgDiffY)
        J11 = cv.boxFilter(imgDiffXX, cv.CV_32F, (duration, bandwidth))
        J22 = cv.boxFilter(imgDiffYY, cv.CV_32F, (duration, bandwidth))
        J12 = cv.boxFilter(imgDiffXY, cv.CV_32F, (duration, bandwidth))
        tmp1 = J11 + J22
        tmp2 = J11 - J22
        tmp2 = cv.multiply(tmp2, tmp2)
        tmp3 = cv.multiply(J12, J12)
        tmp4 = np.sqrt(tmp2 + 4.0 * tmp3)
        lambda1 = tmp1 + tmp4 # biggest eigenvalue
        lambda2 = tmp1 - tmp4  # smallest eigenvalue
        # Coherency is anisotropy degree (consistency of local orientation)
        coherency = cv.divide(lambda1 - lambda2, lambda1 + lambda2)
        return  minmax_scale(coherency, 0., 1.)

    @staticmethod
    def normalize_stft_std(stft):
        """
        Convert a spectrogram to units of standard deviation by computing the mean and standard deviation
        over all values in the spectrogram, then subtracting the mean and dividing by the standard deviation
        for each value.

        :param stft: spectrogram to normalize
        :return normalized: the normalized spectrogram
        """
        mean = np.mean(stft)
        std = np.std(stft)
        normalized = np.subtract(stft, mean)
        normalized = np.divide(normalized, std)
        return normalized

    @staticmethod
    def get_subset(stft, rng, fs=32000):
        """
        Get the subset of spectrogram frequency bins that contain frequencies within the given range.

        :param stft: spectrogram to subset
        :param rng: frequency range
        :param fs: sample rate of the signal from which the stft was computed
        :returns: the subsetted spectrogram
        """
        freq_range = fs / 2
        hz_per_bin = freq_range / len(stft)
        lo_bin = math.floor(rng[0] / hz_per_bin)
        hi_bin = math.ceil(rng[1] / hz_per_bin)
        subsetted = stft[lo_bin:hi_bin + 1]
        return subsetted

    @staticmethod
    def stft_to_dataframe(stft):
        """
        Converts numpy spectrogram to Pandas DataFrame.

        :param stft: the numpy spectrogram
        :returns: stft as a Pandas DataFrame where indexes are timesteps and columns are frequency bins
        """
        dic = OrderedDict()
        for i in range(len(stft)):
            dic[i] = stft[i]
        return pd.DataFrame.from_dict(dic)


if __name__ == "__main__":

    print('Creating song dataset...')
    from data import humpback_songsession as song
    dataset = song.Dataset()
