#!/usr/bin/env python
__author__ = 'Danelle Cline,Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utility for sweeping gamma and number of topics to find the optimum perplexity_sum for a fixed alpha, beta and
vocabulary size

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import os
import math
import tempfile
import conf
import model
import gen_documents
import cluster
import shutil
import visualize
import numpy as np
import tarfile
import pandas as pd
from sklearn import metrics

from data import humpback_songsession as song

def add_plot(model_file):
    with tempfile.TemporaryDirectory() as tmp:
        with tarfile.open(model_file) as tar:
            tar.extractall(path=tmp)

        theta = pd.read_csv(os.path.join(tmp, 'theta.csv'), header=None)
        theta_max_prob = pd.read_csv(os.path.join(tmp, 'theta_max_prob.csv'), header=None)
        secs_per_frame = conf.window_size * (1 - conf.overlap) / conf.sample_rate
        secs_per_doc = secs_per_frame * conf.words_per_doc
        start = 0
        for i, metadata in dataset.metadata.iterrows():
            ms_per_word = int(((metadata.hop_length / metadata.sample_rate) * 1000))
            start_doc = math.floor((start  + conf.seconds_range[0])/ secs_per_doc)
            end_doc = math.ceil((start + conf.seconds_range[1])/ secs_per_doc)
            end_doc = min(end_doc, len(theta))
            theta_doc = theta.values[start_doc:end_doc]
            theta_doc_max = theta_max_prob.values[start_doc:end_doc]
            start_frame = int(secs_per_doc * start_doc / secs_per_frame)
            end_frame = int(secs_per_doc * end_doc / secs_per_frame)
            stft = dataset.spectrogram.values[start_frame:end_frame]
            title = f'{metadata.filename}, {conf.seconds_range[0]} to {conf.seconds_range[1]} secs, msec per word {ms_per_word}'
            visualize.plot(title, stft, metadata, theta_doc, theta_doc_max,
                           target_file=os.path.join(tmp, f'topic_{metadata.filename}_model.png'))
            start += metadata.duration
            np.savetxt(os.path.join(tmp, f'{metadata.filename}_theta.csv'), theta_doc, delimiter=',')
            np.savetxt(os.path.join(tmp, f'{metadata.filename}_theta_max_prob.csv'), theta_doc_max, delimiter=',')

        with tarfile.open(model_file, "w:gz") as tar:
            tar.add(tmp, arcname='.')

if __name__ == '__main__':

    dataset = song.Dataset()
    num_runs = 10
    labels = [[0] * num_runs for i in range(num_runs)]
    model_files = []

    with tempfile.TemporaryDirectory() as tmp_dir:
        for i in range(0, num_runs):
            cluster_file = os.path.join(tmp_dir, f'{i}_similarity_clusters.tar.gz')
            doc_file = os.path.join(tmp_dir, f'{i}_similarity_documents.tar.gz')
            model_file = os.path.join(tmp_dir, f'{i}_similarity_model.tar.gz')

            print('Clustering...')
            vocab_size = conf.vocab_size
            c = cluster.Cluster(cluster_file=cluster_file, dataset=dataset, num_clusters=vocab_size)
            c.start()
            c.join()

            print('Creating documents...')
            d = gen_documents.Document(cluster_file=cluster_file, doc_file=doc_file, metadata=dataset.metadata)
            d.start()
            d.join()

            print('Running topic model...')
            m = model.TopicModel(model_file=model_file, doc_file=doc_file, vocab_size=vocab_size)
            m.start()
            m.join()
            # sample a random number up to 1000 of the maximum probable topics
            # num_samples = min(len(m.max_topic_prob), 1000)
            # sample with fixed seed to have consistent indexes
            # labels[i] = m.max_topic_prob.sample(num_samples, random_state=1).idxmax(axis=1)
            labels[i] = m.max_topic_prob.idxmax(axis=1)
            model_files.append(model_file)


        # compare against all other labels
        similarity = np.array([[0.] * num_runs for i in range(num_runs)])
        for k1, labels_true in enumerate(labels):
            for k2, labels_pred in enumerate(labels):
                if k1 != k2:
                    similarity[k1][k2] = metrics.adjusted_rand_score(labels_true, labels_pred)

        # pick the max and copy
        best1, best2 = np.where(similarity == similarity.max())
        print(f'Best two models {os.path.basename(model_files[best1[0]])} and {os.path.basename(model_files[best2[0]])}')
        add_plot(model_files[best1[0]])
        add_plot(model_files[best2[0]])
        print(f'Copying best to {conf.artifact_path}')
        shutil.copy(model_files[best1[0]], conf.artifact_path)
        shutil.copy(model_files[best2[0]], conf.artifact_path)
