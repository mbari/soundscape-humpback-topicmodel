#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Argument parser for running the topic model

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''
import argparse
import conf as model_conf
from argparse import RawTextHelpFormatter

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('true', 'True'):
        return True
    elif v.lower() in ('false', 'False'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

class ArgParser():

    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter,
                                         description='Run topic model on wav file(s)')
        self.parser.add_argument("--pcen_gain", help="PCEN gain", required=False, type=float, default=model_conf.pcen_gain)
        self.parser.add_argument("--beta", help="beta 0-1", required=False, type=float, default=model_conf.beta)
        self.parser.add_argument("--tau", help="tau 0-1", required=False, type=float, default=model_conf.tau)
        self.parser.add_argument("--alpha", help="alpha 0-1", required=False, type=float, default=model_conf.alpha)
        self.parser.add_argument("--gamma", help="alpha 0-1", required=False, type=float, default=model_conf.gamma)
        self.parser.add_argument("--g_time", help="depth of temporal neighborhood in cell_times", required=False, type=int, default=model_conf.g_time)
        self.parser.add_argument("--topic_sum", help="Number of topics expected. Meaningless if growing topics", required=False, type=int, default=model_conf.num_topics)
        self.parser.add_argument('--grow', type=str2bool, help='Set True to grow topics', const=True, default=False, nargs='?')
        self.parser.add_argument("--window_size", help="size of FFT window for computing the spectrogram", required=False, type=int, default=model_conf.window_size)
        self.parser.add_argument("--overlap", help="FFT window overlap 0-1", required=False, type=self.boolean_string(), default=model_conf.overlap)
        self.parser.add_argument("--online_mint", help="online_mint", required=False, type=int, default=model_conf.online_mint)
        self.parser.add_argument("--out_dir", help="Directory to save output", required=True, type=float, default=model_conf.out_dir)

    def parse_args(self):
        self.args = self.parser.parse_args()
        return self.args

    def boolean_string(self, s):
        if s not in {'False', 'True'}:
            raise ValueError('Not a valid boolean string')
        return s == 'True'

if __name__ == '__main__':
    parser = ArgParser()
    args = parser.parse_args()
    print(args)
