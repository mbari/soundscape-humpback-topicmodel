#!/usr/bin/env python
__author__ = 'Danelle Cline,Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Clusters FFT frames produced by preprocess.py to produce a code book and code words for quantization.

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''
import time
import os
import tempfile
import tarfile
import numpy as np
import pandas as pd
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import KMeans
from sklearn.metrics.pairwise import pairwise_distances
import conf
from util import ensure_dir
from threading import Thread

class Cluster(Thread):

    def __init__(self, dataset, num_clusters, cluster_file=conf.default_cluster_file, cluster_type=conf.cluster_type):
        Thread.__init__(self)
        self._dataset = dataset
        self._cluster_file = cluster_file
        self._labels = pd.DataFrame()
        self._num_clusters= num_clusters
        self._cluster_type = cluster_type
        self._inertia = -1

        # Check whether output path exists and if not create
        ensure_dir(os.path.abspath(cluster_file))

    def run(self):
        """
        Cluster the given data into the number of clusters specified by num_clusters.

        :param df:  data to cluster in a pandas dataframe
        :param num_clusters: the number of clusters
        :param cluster_type: the clustering method to use. Can be "kmeans" for k means or 'mbk' for minibatch kmeans
        :returns:
        """
        if self._cluster_type is 'mbk':
            # mbk parameters:
            # see this stack overflow for reasoning on chosen reassignment ratio value
            # https://stackoverflow.com/questions/21447351/minibatchkmeans-parameters
            reassignment_ratio = 0
            batch_size = 10 * self._num_clusters
            init_size = 3 * self._num_clusters
            clust = MiniBatchKMeans(n_clusters=self._num_clusters, batch_size=batch_size, init_size=init_size,
                                    reassignment_ratio=reassignment_ratio)
            print('Running MiniBatch K-Means with K = {}...'.format(self._num_clusters))

        elif self._cluster_type is 'kmeans':
            clust = KMeans(n_clusters=self._num_clusters, random_state=0)
            print('Running K-Means with K = {}...'.format(self._num_clusters))

        else:
            raise IOError("Invalid cluster_type. Use cluster_type=\'kmeans\' for kmeans or"
                          "cluster_type=\'mbk\' for minibatch kmeans.")

        t0 = time.time()
        c = clust.fit(self._dataset.spectrogram.values)
        print(f'Fit in {(time.time() - t0)} seconds. Inertia {c.inertia_}' )
        self._codebook = pd.DataFrame(c.cluster_centers_)
        self._inertia = c.inertia_

        # clean labels
        self._labels = pd.DataFrame(c.labels_)
        self._labels.fillna(0) #TODO: is zero a good fill value? Do the clusters include zero?
        if  any(np.isnan(self._labels)):
            raise ('invalid code in labels')

        # make dataframes for individual song labels and store in h5 keyed by filename
        metadata = self._dataset.metadata
        with tempfile.TemporaryDirectory() as tmp_dir:
            lo = 0
            print('Saving labels')
            f = os.path.join(tmp_dir, 'labels.h5')
            self._codebook.to_hdf(f, key='codebook', mode='w')
            for _, row in metadata.iterrows():
                hi = lo + int(row.length)
                code_df = pd.DataFrame(self._labels[lo:hi])
                key = os.path.splitext(row.filename)[0]
                code_df.to_hdf(f, key=key)
                lo = hi

            # compress all the results
            print(f'Compressing {tmp_dir} to {self._cluster_file}...')
            with tarfile.open(self._cluster_file, "w:gz") as tar:
                tar.add(tmp_dir, arcname='.')

        print('Done.')

    @property
    def labels(self):
        return self._labels

    @property
    def codebook(self):
        return self._codebook

    @property
    def inertia(self):
        return self._inertia

    def quantize(self, data):
        """

        :param data:
        :param code_book:
        :return:
        """
        distances = pairwise_distances(X=data, Y=self._code_book)
        code = np.argmin(distances, axis=1)
        return pd.DataFrame(code)

if __name__ == "__main__":
    from data import humpback_songsession as song
    c = Cluster(dataset=song.Dataset(), num_clusters=conf.vocab_size)
    c.start()
    c.join()