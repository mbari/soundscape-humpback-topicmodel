#!/usr/bin/env python
__author__ = 'Danelle Cline,Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utility for sweeping alpha, beta and number of topics for a fixed vocabulary size

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import os
import argparse
import wandb
import tempfile
import model
import gen_documents
import cluster
import visualize
import tarfile
import shutil
import pandas as pd
import conf
from data import humpback_songsession as song

api = wandb.Api()

if __name__ == '__main__':
    parser = argparse.ArgumentParser('')
    parser.add_argument('--g_time', type=int, default=100, help='depth of temporal neighborhood in cell_times')
    parser.add_argument('--notes', type=str, default='')

    args = parser.parse_args()

    # Note that entity, project, etc. args are explicitly assigned here for clarity, but do not need to be as
    # environment variables take precedence over these
    run = wandb.init(notes=args.notes, job_type='g_time', \
                     entity=os.environ['WANDB_ENTITY'], project=os.environ['WANDB_PROJECT'], \
                     group=os.environ['WANDB_GROUP'])
    wandb.config.update(args)  # adds all of the arguments as config variables

    with tempfile.TemporaryDirectory() as tmp_dir:
        cluster_file = os.path.join(tmp_dir, f'{run.id}_{args.g_time}_sweep_clusters.tar.gz')
        doc_file = os.path.join(tmp_dir, f'{run.id}_{args.g_time}_sweep_documents.tar.gz')
        model_file = os.path.join(tmp_dir, f'{run.id}_{args.g_time}_model.tar.gz')
        dataset = song.Dataset()

        # Cluster first in case not already complete
        if not os.path.exists(cluster_file):
            c = cluster.Cluster(cluster_file=cluster_file, dataset=dataset, num_clusters=conf.vocab_size)
            c.start()
            c.join()

        # Generate documents in case not already complete
        if not os.path.exists(doc_file):
            d = gen_documents.Document(cluster_file=cluster_file, doc_file=doc_file, metadata=dataset.metadata)
            d.start()
            d.join()

        runs_per_param = 3  # number of runs to average
        topic_sum = 0
        perplexity_sum = 0
        best_perplexity = -1
        fixed = 0.01
        gamma = .001 # control topic growth rate using CRP
        alpha = .01 # control topic growth rate using CRP
        beta = .1 # control topic growth rate using CRP

        # Average all runs and keep track of the best run
        best_model_num_topics = -1
        best_model_out = os.path.join(conf.artifact_path, f'{run.id}_{args.g_time}_model_best.tar.gz')
        for i in range(runs_per_param):
            m = model.TopicModel(alpha=alpha, beta=beta, gamma=gamma, model_file=model_file, doc_file=doc_file,
                                 grow_topics=True, g_time=args.g_time)
            m.start()
            m.join()
            perplexity_sum += m.perplexity
            if best_perplexity < 0:
                best_perplexity = m.perplexity
                best_model_num_topics = m.num_topics_found
                shutil.copy(model_file, best_model_out)
            else:
                if m.perplexity < best_perplexity:
                    best_perplexity = m.perplexity
                    best_model_num_topics = m.num_topics_found
                    shutil.copy(model_file, best_model_out)

            topic_sum += m.num_topics_found

        with tarfile.open(best_model_out) as tar:
            tar.extractall(path=tmp_dir)

        theta = pd.read_csv(os.path.join(tmp_dir, 'theta.csv'), header=None)
        theta_max_prob = pd.read_csv(os.path.join(tmp_dir, 'theta_max_prob.csv'), header=None)
        metadata = dataset.metadata.iloc[0] # assume here all the sound metadata data is the same
        ms_per_word = int(((metadata.hop_length / metadata.sample_rate) * 1000))
        title = f'g_time run {run.id}, {m.num_topics_found} topics found, {conf.target_file_test}, {conf.seconds_range[0]} to {conf.seconds_range[1]} secs, msec per word {ms_per_word}'

        plt = visualize.plot(title, dataset, metadata, theta, theta_max_prob,
                             seconds_range=conf.seconds_range,
                             target_file=os.path.join(tmp_dir, f'{run.id}_topic_example.png'))
        avg_p = perplexity_sum / runs_per_param
        avg_t = topic_sum / runs_per_param

        print(f'g_time {args.g_time} avg_perplexity {avg_p} avg_topics {avg_t} ')
        conf.log(wandb)
        wandb.log({'topic_example': wandb.Image(plt)})
        wandb.log({'ms_per_word': ms_per_word})
        wandb.log({'num_topics': best_model_num_topics})
        wandb.log({'g_time': args.g_time})
        wandb.log({'alpha': alpha})
        wandb.log({'beta': beta})
        wandb.log({'avg_perplexity': avg_p})
        wandb.log({'avg_topics': avg_t})
        wandb.save(best_model_out)
        print('Done')

