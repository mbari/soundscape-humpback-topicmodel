#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Extract VGGish embeddings for collection of wav files

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

from threading import Thread
import numpy as np
import librosa
from sklearn.preprocessing.data import minmax_scale
import tensorflow as tf
import site
import sys
site.addsitedir('./models/research/audioset/vggish')
print(sys.path)
import vggish_slim
import vggish_params
import vggish_input
import vggish_postprocess

class VGGIsh(Thread):

    def __init__(self, hop_seconds, wav_files):
        Thread.__init__(self)
        """
        Create model
        :param hop_seconds: hop size in seconds
        :param wav_files: wav file to compute embeddings of
        :return: 
        """
        self._hop_seconds = hop_seconds
        self._wav_files = wav_files
        self._embeddings = []

    @property
    def embeddings(self):
        return self._embeddings

    def run(self) -> None:
        """
        Run the vgg model, saving embeddings
        """
        tf.compat.v1.reset_default_graph()
        sess = tf.compat.v1.Session()
        vggish_slim.define_vggish_slim()
        vggish_params.EXAMPLE_HOP_SECONDS = self._hop_seconds
        vggish_slim.load_vggish_slim_checkpoint(sess, 'vggish_model.ckpt')
        features_tensor = sess.graph.get_tensor_by_name(vggish_params.INPUT_TENSOR_NAME)
        embedding_tensor = sess.graph.get_tensor_by_name(vggish_params.OUTPUT_TENSOR_NAME)
        for w in self._wav_files:
            x, sr = librosa.load(w)
            input_batch = vggish_input.waveform_to_examples(x, sr)
            [embedding_batch] = sess.run([embedding_tensor],
                                         feed_dict={features_tensor: input_batch})
            p = vggish_postprocess.Postprocessor('vggish_pca_params.npz')
            postprocessed_batch = p.postprocess(embedding_batch)
            print(np.mean(postprocessed_batch))
            print(np.std(postprocessed_batch))
            self._embeddings.append(postprocessed_batch)


if __name__ == "__main__":
    import conf
    if conf.single_file_test:
        wav_files = [conf.wav_path + '/' + conf.target_file_test]
    else:
        import glob
        wav_files = glob.iglob(conf.wav_path + '/' + conf.file_pattern)
    vgg = VGGIsh(hop_seconds=conf.window_size * (1 - conf.overlap) / conf.sample_rate, wav_files=wav_files)
    vgg.start()
    vgg.join()
    print(vgg.embeddings)
