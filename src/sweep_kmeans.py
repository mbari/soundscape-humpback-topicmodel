#!/usr/bin/env python
__author__ = 'Danelle Cline,Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Run k-means and report inertia
Used as part of initial model sweep to determine optimum K
with lowest inertia over different fft window sizes, overlap and denoising algorithms

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''
import os
import argparse
import wandb
import cluster
from data import humpback_songsession as song

if __name__ == '__main__':
    parser = argparse.ArgumentParser('')
    parser.add_argument('--k', type=int, default=20)
    parser.add_argument('--gain', type=float, default=0.98, help='PCEN gain')
    parser.add_argument('--t', type=float, default=1.0, help='PCEN time constant')
    parser.add_argument('--bias', type=float, default=2.0, help='PCEN bias')
    parser.add_argument('--fft', type=int, default=1024, help='FFT window size. Should be a power of 2')
    parser.add_argument('--overlap', type=float, default=0.50, help='FFT window overlap between 0-1.')
    parser.add_argument('--notes', type=str, default='')

    args = parser.parse_args()

    # Note that entity, project, etc. args are explicity assigned here for clarity, but do not need to be as
    # environment variables take precedence over these
    wandb.init(notes=args.notes, job_type='kmeans', \
                         entity=os.environ['WANDB_ENTITY'], project=os.environ['WANDB_PROJECT'], \
                         group=os.environ['WANDB_GROUP'])
    wandb.config.update(args)  # adds all of the arguments as config variables
    dataset = song.Dataset(num_fft=args.fft, overlap=args.overlap, gain=args.gain, t=args.t, bias=args.bias)
    c = cluster.Cluster(dataset=dataset, num_clusters=args.k)
    c.start()
    c.join()
    print(f'Inertia {c.inertia}')
    wandb.log({'inertia': c.inertia})
    wandb.log({'k': args.k})
    wandb.log({'fft': args.fft})
    wandb.log({'overlap': args.overlap})

