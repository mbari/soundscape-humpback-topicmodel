from data import input_data
import preprocess as p
import os
import conf

class Dataset():

    def __init__(self, wav_path=conf.wav_path, artifact_path=conf.artifact_path, num_fft=conf.window_size, overlap=conf.overlap,
                 gain=conf.pcen_gain, t=conf.pcen_time_constant,
                 bias=conf.pcen_bias, metadata_only=False):
        pcen_key = f'{int(gain*10)}_{int(conf.pcen_power*100)}_{bias}_{int(t*10)}'
        out_file = os.path.join(artifact_path,  f'preprocess_{num_fft}_{int(overlap*100)}_{pcen_key}.tar.gz')
        if not os.path.exists(out_file):
            p.Preprocess(in_dir=wav_path, out_file=out_file, gain=gain, time_constant=t, bias=bias)
        if metadata_only:
            self._metadata, _ = input_data.read(out_file)
        else:
            self._metadata, self._spectrogram = input_data.read(out_file)

    @property
    def spectrogram(self):
        return self._spectrogram

    @property
    def metadata(self):
        return self._metadata

if __name__ == '__main__':
    data = Dataset()
    print(data.spectrogram)
