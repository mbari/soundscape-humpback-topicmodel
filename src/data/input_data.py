#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utility module for loading precomputed data

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''
import numpy as np
import struct
import gzip
import shutil
import pandas as pd
import tempfile
import os
import tarfile
import glob

def read(tar_file):
    '''
    Read spectrogram and metadata as numpy array from compressed file
    :param tar_file: compressed tar file with data
    :return: tuple (metadata dataframe, spectrogram dataframe)
    '''
    with tempfile.TemporaryDirectory() as tmp_dir:
        print(f'Unpacking {tar_file} to {tmp_dir}')
        metadata_df_final = pd.DataFrame()
        spec_df_final = pd.DataFrame()
        with tarfile.open(tar_file) as tar:
            tar.extractall(path=tmp_dir)
            for f in glob.iglob(os.path.join(tmp_dir, '*.h5')):
                metadata_df = pd.read_hdf(f, 'metadata_df')
                spec_df = pd.read_hdf(f, 'spec_df')
                spec_df_final = spec_df_final.append(spec_df, ignore_index=True)
                metadata_df_final = metadata_df_final.append(metadata_df, ignore_index=True)

        tar.close()
    return metadata_df_final, spec_df_final
