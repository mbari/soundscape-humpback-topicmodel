#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utilities for extracting topics

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import conf
import pandas as pd
import matplotlib
import click
import glob
matplotlib.use('agg')
import matplotlib.pyplot as plt
import os
import math
import numpy as np
from util import write_csv
import librosa
from librosa import display
from data import humpback_songsession as song
from sklearn.preprocessing.data import minmax_scale
from vggish import VGGIsh
import tempfile
import tarfile

@click.command(help="Extract representations of topics from a model file")
@click.option("--topic",  type=click.INT)
@click.option("--model-file",  type=click.STRING)
def extract(topic, model_file):

    font = {'family': 'Times',
            'weight': 'normal',
            'size': 10}
    matplotlib.rc('font', **font)  # font for everything
    plt.rc('legend', fontsize=6)  # smaller font for legend

    dataset = song.Dataset()
    fs = dataset.metadata.iloc[0].sample_rate

    with tempfile.TemporaryDirectory(dir='/tmp') as tmp_dir:
        with tarfile.open(os.path.join(conf.artifact_path, model_file)) as tar:
            tar.extractall(path=tmp_dir)

        theta = pd.read_csv(os.path.join(tmp_dir, 'theta.csv'), header=None)
        perplexity = pd.read_csv(os.path.join(tmp_dir, 'perplexity.csv'), header=None)[1]
        # normalize perplexity by the max perplexity found
        perplexity /= perplexity.values.max()

        # calculate fft and model parameters
        secs_per_frame = conf.window_size * (1 - conf.overlap) / conf.sample_rate
        secs_per_doc = secs_per_frame * conf.words_per_doc

        print(f'Creating topic {topic}...')
        topic_dir = f'{conf.artifact_path}/topic{topic:02d}'
        if not os.path.exists(topic_dir):
            os.makedirs(topic_dir)

        # smooth and binarize the topics
        df = theta.iloc[:,topic]
        theta_discrete = df.rolling(3, min_periods=1).mean()
        theta_discrete[theta_discrete >= 0.25] = 1
        theta_discrete[theta_discrete < 0.25] = 0

        # get indices of each segment of consecutively equal values - these are potential topics
        blocks = (theta_discrete != theta_discrete.shift()).cumsum()
        df = pd.DataFrame(blocks.values, columns=['values'])
        indexes = df.groupby('values').apply(lambda x: (x.index[0], x.index[-1]))

        end_index = theta_discrete.index[-1]

        # find the average width of the topic and store the indexes
        widths = []
        for g in indexes:
            length = g[1] - g[0] + 1
            b = theta_discrete[g[0]]
            print(f'{b} g[0] {g[0]} g[1] {g[1]} ')
            if theta_discrete[g[0]] == 1.0 and length > 0:
                if g[0] < 0 or g[1] > end_index:
                    print('skipping')
                    continue
                widths.append(g[1] - g[0])

        valid_topics = len(widths)
        num_show = min(5, valid_topics)
        if valid_topics == 0:
            print('No valid topics found to plot')
            exit(-1)

        fig, axs = plt.subplots(8, num_show, gridspec_kw = {'wspace':0, 'hspace':0}, figsize=(6, 10))
        avg_width = math.ceil(np.mean(widths))
        theta_sum = np.zeros((avg_width, theta.shape[1]), dtype=float)
        df_topics = pd.DataFrame()
        example_num = 0
        for g in indexes:
            length = g[1] - g[0] + 1
            b = theta_discrete[g[0]]
            print(f'{b} g[0] {g[0]} g[1] {g[1]} ')
            if theta_discrete[g[0]] == 1.0 and length > 1:
                if g[0] < 0 or g[1] > end_index:
                    print('skipping')
                    continue

                center_topic = math.ceil(g[0] + length/2)
                start_topic = int(max(math.floor(center_topic - avg_width/2), 0))
                end_topic = int(min(math.floor(center_topic + avg_width/2), end_index))

                if end_topic - start_topic == avg_width:
                    np.add(theta_sum, theta[start_topic:end_topic],theta_sum)

                    if example_num < num_show:
                        start_time = start_topic * secs_per_doc
                        end_time = end_topic * secs_per_doc

                        start_frame = int(start_time / secs_per_frame)
                        end_frame = int(end_time / secs_per_frame)

                        df_topics = df_topics.append({'start':start_topic * secs_per_doc,
                                                      'end':end_topic * secs_per_doc,
                                                      'start_topic': start_topic,
                                                      'end_topic': end_topic},
                                                     ignore_index=True)

                        sub = dataset.spectrogram.values[start_frame:end_frame]
                        librosa.display.specshow(np.transpose(sub),ax=axs[1,example_num],
                                                 hop_length=int((1 - conf.overlap) * conf.window_size),
                                                 sr=conf.sample_rate, fmin=conf.freq_range[0], fmax=conf.freq_range[1],
                                                 x_axis='time', y_axis='mel', cmap='Blues')

                        axs[2, example_num].plot(perplexity[start_topic:end_topic])
                        np.savetxt(topic_dir + f'topictheta{topic:02d}.csv', theta[g[0]:g[1]],
                                      delimiter=",")
                example_num += 1

        theta_mean = theta_sum / valid_topics
        offset = 0
        tmp_topic_dir = os.path.join(tmp_dir, 'topics')
        os.makedirs(tmp_topic_dir)
        for i, r in dataset.metadata.iterrows():
            data, sr = librosa.load(os.path.join(conf.wav_path, r.filename), sr=None)
            x = minmax_scale(data, feature_range=(-1., 1.))
            print(f'Saving wav topics from {r.filename}...')
            for j, t in df_topics.iterrows():
                topic_times = df_topics[(df_topics.start >= t.start) &
                                        (df_topics.end <= t.end)]
                for k, times in topic_times.iterrows():
                    # signal
                    y = np.array(x[int((times.start - offset)*fs): int((times.end - offset)*fs)])
                    librosa.output.write_wav(os.path.join(tmp_topic_dir, f'{k}.wav'), y, sr)
            offset += r.duration

        print('Computing embedding mean...')
        hop_seconds = conf.window_size * (1 - conf.overlap) / conf.sample_rate
        vgg = VGGIsh(hop_seconds=hop_seconds, wav_files=glob.glob(tmp_topic_dir + '/*.wav'))
        vgg.start()
        vgg.join()
        embeddings = np.array(vgg.embeddings)
        embedding_mean = embeddings.mean(axis=0).astype(np.float32)

        # Theta and embedding coherence
        for i, r in dataset.metadata.iterrows():
            x, _ = librosa.load(os.path.join(conf.wav_path, r.filename), sr=None)
            for j, t in df_topics.iterrows():
                topic_times = df_topics[(df_topics.start >= t.start) &
                                        (df_topics.end <= t.end)]
                for k, times in topic_times.iterrows():
                    t_k = np.array(theta.iloc[int(times.start_topic):int(times.end_topic)])
                    t_topic = np.transpose(t_k[:, topic])

                    # invert perplexity
                    p_topic = perplexity.iloc[int(times.start_topic):int(times.end_topic)]

                    # compute perplexity-theta weight
                    pt_weight = ((1-p_topic) * t_topic).values
                    axs[3, k].plot(t_topic)
                    axs[4, k].plot(pt_weight)

                    em = embeddings[k].astype(np.float32)
                    p_dates = pd.date_range(start=times.start, periods=len(pt_weight),
                                             freq=f'{int(secs_per_doc * 1e6)}us')
                    em_dates = pd.date_range(start=times.start, periods=int(em.shape[0]),
                                             freq=f'{int(hop_seconds * 1e6)}us')
                    em_df = pd.DataFrame(data={'enorm': np.ones(em.shape[0])}, index=em_dates)
                    py_df = pd.DataFrame(data={'py': pt_weight}, index=p_dates).reindex(em_df.index, method='nearest')

                    # interpolate onto same dimension as the embedding
                    py_df_i = py_df.interpolate(axis='index')

                    # create matrix of perplexity with same dimension as embedding vector 128
                    py_mat = np.array([py_df_i.values] * em.shape[1])
                    py_mat_t = np.transpose(py_mat)

                    # weight embedding with the perplexity and the theta coherence
                    em_best = em * np.squeeze(py_mat_t.astype(np.float32))
                    em_best_mean = embedding_mean * np.squeeze(py_mat_t.astype(np.float32))

                    diff_mean = em_best_mean - em_best
                    # https://en.wikipedia.org/wiki/Index_of_dispersion need to verify this is correct
                    dispersion = np.var(diff_mean)/np.mean(em_best)
                    print(f'===dispersion {dispersion}')

                    axs[5, k].imshow(np.transpose(em), aspect='auto', cmap='binary')
                    axs[6, k].imshow(np.transpose(diff_mean),  aspect='auto', cmap='binary')
                    axs[7, k].imshow(np.transpose(embedding_mean),  aspect='auto', cmap='binary')
                    axs[5, k].grid(False)
                    axs[6, k].grid(False)

                    images = axs[0, k].imshow(np.ones(diff_mean.shape)*dispersion, aspect='auto', cmap='Blues')
            offset += int(r.duration*fs)

        for i in range(0, num_show):
            for j in [0, 1, 2, 3, 4, 5, 6, 7]:
                if i > 0:
                    axs[j, i].set_xlabel('')
                    axs[j, i].set_ylabel('')
                    axs[j, i].set_xticklabels([])
                    axs[j, i].set_yticklabels([])
                # axs[j, i].autoscale(False)
            for j in [2, 3, 4]:
                axs[j, i].set_ylim([0., 1.2])
                axs[j, i].set_ylim([0., 1.2])
                axs[j, i].set_ylim([0., 1.2])

        # fix the title and labels for first column only
        axs[0, 0].set_ylabel('Dispersion', rotation=0)
        axs[1, 0].set_ylabel('Hz', rotation=0)
        axs[2, 0].set_ylabel('Perplexity=P', rotation=0)
        axs[3, 0].set_ylabel(r'$\theta$ ', rotation=0)
        axs[4, 0].set_ylabel(r'$\theta$*(1 - P)', rotation=0)
        axs[6, 0].set_ylabel(r'$\theta$*(1 - P)*E-Emean', rotation=0)
        axs[7, 0].set_ylabel(r'Emean', rotation=0)
        axs[5, 0].set_ylabel('Embedding=E', rotation=0)

        fig.colorbar(images, ax=axs[0, 0], orientation='vertical')
        fig.suptitle(f'Topic {topic}\naverage duration {avg_width*secs_per_doc:3.2f} seconds')
        # plt.tight_layout()
        fig.savefig(topic_dir + f'/topic{topic:02d}.png', dpi=120)
        write_csv(theta_mean/valid_topics, topic_dir + '/topicthetamean.csv')
        print('Done!')

if __name__ == '__main__':
    extract()