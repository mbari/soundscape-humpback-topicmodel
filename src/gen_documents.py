#!/usr/bin/env python
__author__ = 'Danelle Cline,Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Utility for converting labels to documents

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import glob
import numpy as np
import pandas as pd
import conf
import os
import tempfile
import tarfile
from threading import Thread
from util import write_csv
from util import ensure_dir

class Document(Thread):

    def __init__(self, metadata, cluster_file=conf.default_cluster_file, doc_file=conf.default_doc_file,
                 words_per_doc=conf.words_per_doc):
        Thread.__init__(self)
        # this assumes all data has the same sample rate and hop length in the collection
        self._ms_per_word = int((metadata.iloc[0].hop_length* 1000)/ metadata.iloc[0].sample_rate)
        self._words_per_doc = words_per_doc
        self._doc_file = doc_file
        self._cluster_file = cluster_file
        self._metadata = metadata
        ensure_dir(os.path.abspath(doc_file))

    def run(self) -> None:

        timestamp = 0
        increments = self._ms_per_word * self._words_per_doc

        with tempfile.TemporaryDirectory() as tmp_dir:
            print(f'Unpacking {self._cluster_file} to {tmp_dir}')

            with tarfile.open(self._cluster_file) as tar:
                tar.extractall(path=tmp_dir)

            print(f'Writing {self._doc_file} with  {increments} millisecond timestamp increments...')
            with open(os.path.join(tmp_dir, 'documents.csv'), 'w') as out:
                for _, row in self._metadata.iterrows():
                        print(f'Making documents with {self._words_per_doc} words for {row.filename}...')
                        key = os.path.splitext(row.filename)[0]
                        labels_df = pd.read_hdf(os.path.join(tmp_dir, 'labels.h5'), key)
                        docs = self.group_docs(words=labels_df)
                        for words in docs:
                            word_array = np.array(words[1:])
                            word_array = np.nan_to_num(word_array)
                            if len(word_array) != self._words_per_doc:
                                print(f'invalid word length {len(word_array)}. Coulld be the end of the file')
                                continue
                            l = word_array.tolist()
                            csv_str_word_array = ','.join([str(int(a)) for a in l])
                            line = f'{timestamp},{csv_str_word_array}\n'
                            out.write(line)
                            print(line)
                            timestamp += increments

            # clean and compress the results
            os.remove(os.path.join(tmp_dir, 'labels.h5'))
            print(f'Compressing {tmp_dir} to {self._doc_file}...')
            with tarfile.open(self._doc_file, "w:gz") as tar:
                tar.add(tmp_dir, arcname='.')

        print('Done.')


    def group_docs(self, words, start_t=0):
        '''
        :param words:  words to collapse into array topic model expects
        :param start_t:  starting index
        :return:
        '''
        docs = []
        i = 0
        j = self._words_per_doc
        while i < len(words):
            timestamp_ms = (j * self._ms_per_word) + start_t
            d = [timestamp_ms]
            d.extend(list(words[0][i:j]))
            docs.append(d)
            i += self._words_per_doc
            if (j + self._words_per_doc) <= len(words):
                j += self._words_per_doc
            else:
                j = len(words)
        return docs


def label_docs(doc_times, event_times, event_labels):
    j = 0  # docs index
    doc_labels = np.zeros(len(doc_times))

    for i in range(len(event_times)):
        # start and end of event
        t0 = event_times.loc[i][0]
        t1 = event_times.loc[i][1]

        while doc_times[j] <= t0:
            j += 1  # increment doc

        while doc_times[j] <= t1:
            doc_labels[j] = event_labels.loc[i]
            j += 1  # increment doc

        doc_labels[j] = event_labels.loc[i]
        j += 1

    return doc_labels



if __name__ == "__main__":
    from data import humpback_songsession as song
    dataset = song.Dataset(metadata_only=True)
    d = Document(metadata=dataset.metadata)
    d.start()
    d.join()

