import pandas as pd
import matplotlib.pyplot as plt
import os
import conf


def plot_gamma(desc, ax, filename):
    df = pd.read_csv(filename)
    df = df.sort_values(by='gamma')
    for i, d in df.iterrows():
        ax.plot(d['gamma'], d['perplexity_sum'], label="{} {} topics".format(desc, d['topic_sum']), marker='*')

    ax.set_title("Cell neighborhood={} Gamma=[0.5, 0.1, 0.01, 0.001, .0001]\nwith Beta={}, Alpha={}".format(conf.g_time,
                                                                                                            conf.alpha,
                                                                                                            conf.beta))
    ax.set_xscale('log')
    ax.set_xlabel('Gamma')
    ax.set_yscale('log')
    ax.set_ylabel('Perplexity')
    ax.legend(loc=1)


def plot_alphabeta(desc, ax, filename, marker):
    df = pd.read_csv(filename)
    sweep_df = df[['perplexity_sum', 'alpha', 'beta', 'topic_sum']]
    # beta_df = sweep_df.loc[sweep_df['alpha'] == 0.001]
    # alpha_df = sweep_df.loc[sweep_df['beta'] == 0.001]

    alpha_df = sweep_df.loc[0:((len(sweep_df) / 2) - 1)]
    beta_df = sweep_df.loc[len(sweep_df) / 2:len(sweep_df)]

    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))

    for topic in [2, 5, 10]:  # , 15]:  #20, 50, 100]:
        alphas = alpha_df.loc[alpha_df['topic_sum'] == topic].sort_values(by='alpha')
        betas = beta_df.loc[beta_df['topic_sum'] == topic].sort_values(by='beta')

        ax[0].plot(alphas['alpha'],
                   alphas['perplexity_sum'],
                   label="{} {} topics".format(desc, topic), marker=marker)

        ax[1].plot(betas['beta'],
                   betas['perplexity_sum'],
                   label="{} {} topics".format(desc, topic), marker=marker)

    ax[0].set_title("Cell neighborhood={} Alpha=[0.001, 0.01, 0.1, 0.5, 0.9]\nwith Beta=0.1".format(conf.g_time))
    ax[0].set_xscale('log')
    ax[0].set_xlabel('Alpha')
    ax[0].set_yscale('log')
    ax[0].set_ylabel('Perplexity')
    ax[0].legend(loc=1)

    ax[1].set_title("Cell neighborhood={} Beta=[0.001, 0.01, 0.1, 0.5, 0.9]\nAlpha=0.1".format(conf.g_time))
    ax[1].set_xscale('log')
    ax[1].set_xlabel('Beta')
    ax[1].set_yscale('log')
    ax[1].set_ylabel('Perplexity')
    ax[1].legend(loc=1)


if __name__ == "__main__":
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
    markers = ['*', '+']
    filenames = ['PCEN_param_sweep_1024_win_50_ovr_32_wpd_3_gtime.csv',
                 'STD_param_sweep_1024_win_50_ovr_32_wpd_0_gtime.csv']
    filenames = ['param_sweep_1024_win_50_ovr_32_wpd_1_gtime.csv']
    markers = ['*', '+']
    '''for i,f in enumerate(filenames):
        desc = f.split('_')[0]
        plot_alphabeta(desc, ax,  os.path.join(conf.out_dir, f), markers[i])

    plt.savefig(os.path.join(conf.out_dir, 'param_sweep_1024_win_50_ovr_32_wpd_3_gtime.png'),bbox_inches='tight')
    '''
    fig = plt.figure(figsize=(10, 5))

    ax = fig.add_subplot(1, 1, 1)
    filename = 'PCEN_param_sweep_1024_win_50_ovr_32_wpd_1_gtimegamma'
    desc = 'PCEN_param_sweep_1024_win_50_ovr_32_wpd_1_gtimegamma'
    plot_gamma(desc, ax, os.path.join(conf.out_dir, filename + '.csv'))
    plt.savefig(os.path.join(conf.out_dir, filename + '.png'), bbox_inches='tight')
