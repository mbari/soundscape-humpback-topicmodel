#!/usr/bin/env python
__author__ = 'Danelle Cline'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Configuration file for humpback song topic analysis

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''
import os

# default dataset
dataset = 'humpback_songsession'

# default file/directory paths
wav_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../', 'data/wav/16kHz/'))
artifact_path = os.path.abspath(os.path.join(os.path.dirname(__file__), '../', 'data/artifacts/16kHz/'))
default_cluster_file = os.path.join(artifact_path, 'clusters.tar.gz')
default_doc_file = os.path.join(artifact_path, 'documents.tar.gz')
default_model_file = os.path.join(artifact_path, 'model.tar.gz')
default_topic_output_file = os.path.join(artifact_path, 'topic_example.png')

sample_rate = 16000

# for testing with a single file which is must faster for working through pipeline changes
single_file_test = False
# target_file_test = "MARS_20161221_000046_SongSession_16kHz_HPF5Hz"
# target_file_test = "MARS_20161221_000046_SongSession_16kHz_HPF5Hz_noise"
# file_pattern = 'MARS_'
# target_file_test = 'HBSe_20151207T070362_15s.wav'
# target_file_test = 'tones_concat_midchange.wav'
# target_file_test is ignored with the file_pattern is specified
target_file_test = 'AAC_1_hb.wav'
# file_pattern = 'tones_concat_midchange'
file_pattern = 'HBUnit*.wav'

# visualization parameters
# seconds_range = (6000, 6300) # for MARS_
#seconds_range = (0, 120) # for HBSe_
# seconds_range = (0, 18) # for tones_2000midchange
seconds_range = (0, 9)
# Name of Docker image with Topic Model
rost_docker = "mbari/rost"

# spectrogram parameters
use_pcen = True
use_coherence = False

window_size = 4096
overlap = 0.95
coherence_bandwidth = 3
coherence_duration = 5
coherence_key = f'{coherence_bandwidth}_{coherence_duration}'
power = True
freq_range = (50, int(sample_rate/2))
sigma = 1 #gaussian filter sigma
pcen_gain = 0.25
pcen_time_constant = 0.4
pcen_power = 0.5
pcen_bias = 2
pcen_epsilon = 10e-6

# clustering parameters
vocab_size = 4000 #
whiten = None  # 'pca', 'std', or None
cluster_type = 'mbk'

# doc parameters
# each word is an fft slice
# each document is a collection of words ( words_per_doc )
words_per_doc = 15
doc_file = 'label.csv'

# model parameters
num_topics = 30 # this is meaningless if growing topics
alpha = 0.01
beta = 0.1
g_time = 5 #depth of temporal neighborhood in cell_time
cell_space = 0
# Do online learning; i.e., output topic labels after reading each document/cell.
online = False
online_mint = 100 # default in ROST is 100
# ratio of local refinement (vs global refinement) in grow_topics
tau = 0.20 #this will change the exponential factor applied to label assignment in temporal neighborhood.
# Run
grow_topics = True
gamma = .001 #control topic growth rate using  CRP
iterations = 1000

# log wandb default parameters
def log(wandb):
    wandb.log({'iterations': iterations})
    wandb.log({'words_per_doc': words_per_doc})
    wandb.log({'num_topics': num_topics})
    wandb.log({'alpha': alpha})
    wandb.log({'beta': beta})
    wandb.log({'gamma': gamma})
    wandb.log({'g_time': g_time})
    wandb.log({'cell_space': cell_space})
    wandb.log({'online': online})
    wandb.log({'online_mint': online_mint})
    wandb.log({'tau': tau})
    wandb.log({'grow_topics': grow_topics})
    wandb.log({'k': vocab_size})
    wandb.log({'fft': window_size})
    wandb.log({'overlap': overlap})
    wandb.log({'pcen_gain': pcen_gain})
    wandb.log({'pcen_time_constant': pcen_time_constant})
    wandb.log({'pcen_power': pcen_power})
    wandb.log({'pcen_bias': pcen_bias})
    wandb.log({'use_pcen': use_pcen})
    wandb.log({'use_coherence': use_coherence})
    wandb.log({'coherence_bandwidth': coherence_bandwidth})
    wandb.log({'coherence_duration': coherence_duration})
