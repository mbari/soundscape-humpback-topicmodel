#!/usr/bin/env python
__author__ = 'Danelle Cline, Thomas Bergamaschi'
__copyright__ = '2020'
__license__ = 'GPL v3'
__contact__ = 'dcline at mbari.org'
__doc__ = '''

Topic model abstraction for running dockerized implementation of the ROST topic model

@var __date__: Date of last svn commit
@undocumented: __doc__ parser
@status: production
@license: GPL
'''

import pandas as pd
import numpy as np
import docker
import conf
import os
import tempfile
import tarfile
from util import ensure_dir
from util import write_csv
from threading import Thread


class TopicModel(Thread):


    def __init__(self, model_file=conf.default_model_file, doc_file=conf.default_doc_file, rost_docker=conf.rost_docker,
                 vocab_size=conf.vocab_size, num_topics=conf.num_topics, alpha=conf.alpha, beta=conf.beta,
                 g_time=conf.g_time, online=conf.online, online_mint=conf.online_mint,
                 grow_topics=conf.grow_topics, gamma=conf.gamma):
        """
        :param rost_docker: name of ROST docker image
        :param model_file: full path to compressed file where model should be saved
        :param doc_file: full path to compressed document file
        :param vocab_size: vocabulary size
        :param num_topics: number of topics
        :param self._alpha: sparsity of theta
        :param self._beta: sparsity of phi
        :param threads: number of threads
        :param g_time: depth of temporal neighborhood
        :param online: run ROST online
        :param online_mint: min time (in ms) to spend between observation time steps
        :param grow_topics: grow topics using Chinese Restaurant Process (CRP)
        :param grow_topic_size: max number of topics to grow using CRP
        :param gamma: controls topic growth rate with CRP
        """
        Thread.__init__(self)
        self._rost_docker = rost_docker
        self._model_file = model_file
        self._rost_docker = rost_docker
        self._doc_file = doc_file
        self._vocab_size = vocab_size
        self._num_topics = num_topics
        self._alpha = alpha
        self._beta = beta
        self._g_time = g_time
        self._online = online
        self._online_mint = online_mint
        self._grow_topics = grow_topics
        self._gamma = gamma
        if not self._grow_topics:
            self._num_topics_found = num_topics
        else:
            self._num_topics_found = -1
        self._perplexity = -1
        ensure_dir(os.path.abspath(model_file))

    @property
    def max_topic_prob(self):
        return self._max_topic_prob

    @property
    def perplexity(self):
        return self._perplexity

    @property
    def num_topics_found(self):
        return self._num_topics_found

    def run(self) -> None:

        try:
            client = docker.from_env()

            # Create volume to store results
            volume = client.volumes.create()

            # Extract data from tar
            with tempfile.TemporaryDirectory(dir=os.environ['HOME']) as tmp_dir:
                print(f'Unpacking {self._doc_file} to {tmp_dir}')

                with tarfile.open(self._doc_file) as tar:
                    tar.extractall(path=tmp_dir)

                # Copy document to volume
                print(f'Copying data in {tmp_dir} into docker volume')
                c = client.containers.run('ubuntu:20.04', "/bin/bash -exec 'cp /in/* /io/'",
                                          volumes={
                                              tmp_dir: {'bind': '/in', 'mode': 'ro'},
                                              volume.name: {'bind': '/io', 'mode': 'rw'}
                                          },
                                          detach=True,  auto_remove=False,  privileged=True,
                                          stdout=True, stderr=True)
                self.monitor(c)

            documents = os.path.join('/io', 'documents.csv')
            max_likelihood = os.path.join('/io', 'topics.maxlikelihood.csv')
            topics = os.path.join('/io', 'topics.csv')
            topic_model = os.path.join('/io', 'topicmodel.csv')
            topic_model_tmp = os.path.join(tmp_dir, 'topicmodel.csv')
            topic_model_histogram = os.path.join('/io', 'topics.hist.csv')
            topic_model_histogram_tmp = os.path.join(tmp_dir, 'topics.hist.csv')
            perplexity = os.path.join('/io', 'perplexity.csv')
            perplexity_tmp = os.path.join(tmp_dir, 'perplexity.csv')
            log_file = os.path.join('/io', 'topics.log')

            command = f'topics.refine.t -i  {documents}  --out.topics={topics} ' \
                      f'--out.topics.ml={max_likelihood}  --out.topicmodel={topic_model} ' \
                      f'--ppx.out={perplexity}  --logfile={log_file} -V {self._vocab_size} ' \
                      f'--alpha={self._alpha}  --beta={self._beta}  --threads=6  --g.time={self._g_time} ' \
                      f'--tau={conf.tau}  --iter={conf.iterations} --cell.space={conf.cell_space} '

            if self._online:
                online = os.path.join('/io', 'topics.online.csv')
                perplexity = os.path.join('/io', 'perplexity.online.csv')
                command += f'--online  --out.topics.online={online} --out.ppx.online={perplexity} ' \
                           f'--online.mint={self._online_mint}'

            if self._grow_topics:
                command += f' --grow.topics.size=true  --gamma={self._gamma}'
            else:
                command += f' -K {self._num_topics} '

            # Run the topic model
            c = client.containers.run(self._rost_docker, command, volumes={
                volume.name: {'bind': '/io', 'mode': 'rw'}},
                                      detach=True, auto_remove=False, privileged=True, stdout=True, stderr=True)
            self.monitor(c)

            # Run the bin count command
            command = f'words.bincount  -i  {max_likelihood}  -o {topic_model_histogram} -V {self._num_topics}'
            c = client.containers.run(self._rost_docker, command, volumes={
                volume.name: {'bind': '/io', 'mode': 'rw'}},
                                      detach=True, auto_remove=False, privileged=True, stdout=True, stderr=True)
            self.monitor(c)

            # Copy results from model to temp dir
            print(f'Copying data in {tmp_dir} into docker volume')
            c = client.containers.run('ubuntu:20.04', "/bin/bash -exec 'cp /io/* /out/'",
                                      volumes={
                                          tmp_dir: {'bind': '/out', 'mode': 'rw'},
                                          volume.name: {'bind': '/io', 'mode': 'ro'}
                                      },
                                      detach=True, auto_remove=False, privileged=True,
                                      stdout=True, stderr=True)
            self.monitor(c)

            topic_model = pd.read_csv(topic_model_tmp, header=None).values
            topic_hist = pd.read_csv(topic_model_histogram_tmp, header=None).drop(0, axis=1).values

            if self._grow_topics:
                # read topics skipping over the index column
                self._num_topics_found = len(np.unique(topic_model[1:-1]))
                print(f'Found {self._num_topics_found} unique topics')

            num_documents = len(topic_hist)  # number of documents
            theta = np.zeros((num_documents, self._num_topics))

            for d in range(num_documents):
                denominator = np.sum(topic_hist[d]) + (self._num_topics * self._alpha)
                for z in range(self._num_topics):
                    numerator = topic_hist[d][z] + self._alpha
                    theta[d][z] = numerator / denominator

            p = os.path.join(tmp_dir, 'theta.csv')
            write_csv(theta, p)
            theta_pd = pd.read_csv(p, header=None)
            self._max_topic_prob = self.max_topic_probability(theta_pd)
            # save skipping over the first column of indexes and dropping the header - this makes this exactly like
            # the topic model output
            self._max_topic_prob.to_csv(os.path.join(tmp_dir, 'theta_max_prob.csv'), index=False, header=None)

            df = pd.read_csv(filepath_or_buffer=perplexity_tmp,  header=None)
            self._perplexity = df.sum()[1]/len(df)
            print(f'average per document perplexity = {self._perplexity}')

            # clean and compress the results
            os.remove(os.path.join(tmp_dir, 'documents.csv'))
            print(f'Compressing {tmp_dir} to {self._model_file}...')
            with tarfile.open(self._model_file, "w:gz") as tar:
                tar.add(tmp_dir, arcname='.')

        except Exception as ex:
            raise Exception(ex)
        finally:
            client.volumes.prune()

    @staticmethod
    def monitor(container):
        """
        Monitor running container and print output
        :param container:
        :return:
        """
        container.reload()
        l = ""
        # TODO: add stop signal handle
        while True:
            for line in container.logs(stream=True):
                l = line.strip().decode()
                print(l)
            else:
                break
        # return the last line for pattern matching file start/end
        return l


    def max_topic_probability(self, theta):
        """
        Smooth and find the maximum topic probability over the event
        :param theta table
        :return: smoothed theta with max prob assignments
        """
        num_topics = theta.shape[1]
        theta_smoothed = pd.DataFrame(0., index=np.arange(len(theta)), columns=[i for i in range(num_topics)])

        for t in range(num_topics):
            print(f'Creating topic {t} of {num_topics}...')
            df = theta.iloc[:, t]

            # Smooth and binarize the topics
            theta_topic = df.rolling(2, min_periods=1).mean()
            theta_topic[theta_topic >= 0.5] = 1
            theta_topic[theta_topic < 0.5] = 0

            # Get indices of each segment of consecutively equal values
            blocks = (theta_topic != theta_topic.shift()).cumsum()
            df_blocks = pd.DataFrame(blocks.values, columns=['values'])
            indexes = df_blocks.groupby('values').apply(lambda x: (x.index[0], x.index[-1]))

            # Add indexes back in theta data frame setting the topic probability to the max over the event
            for g in indexes:
                start_index = g[0]
                end_index = g[1]
                # length = end_index - start_index + 1
                if theta_topic[start_index] == 1.0:  # and length > 1:
                    unit_max_prob = df[start_index:end_index].max()
                    for index in np.arange(start_index, end_index):
                        theta_smoothed[t][index] = df[index]
                        # print(f'setting max probability {unit_max_prob} for topic {t} length {length} {g} ')
                else:
                    for index in np.arange(start_index, end_index):
                        theta_smoothed[t][index] = 0

        return theta_smoothed


if __name__ == "__main__":
    t = TopicModel()
    t.start()
    t.join()

